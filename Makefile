COMPILEFLAGS= -g
CCOMP=gcc

prog: isPrime.o nextPrime.o header.h printSolution.o main.o
	$(CCOMP) $(COMPILEFLAGS) -o prog isPrime.o nextPrime.o main.o printSolution.o
main.o: main.c isPrime.c nextPrime.c header.h printSolution.c
	$(CCOMP) $(COMPILEFLAGS) -c main.c isPrime.c nextPrime.c printSolution.c
nextPrime.o: nextPrime.c isPrime.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c isPrime.c nextPrime.c 
printSolution.o: printSolution.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c printSolution.c
isPrime.o: isPrime.c header.h
	$(CCOMP) $(COMPILEFLAGS) -c isPrime.c
clean: 
	rm isPrime.o nextPrime.o printSolution.o main.o prog
