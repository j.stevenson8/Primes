#include "header.h"

/*
//This is a testbed for nextPrime
void main()
{
	int test,result;
	char input[100];
	printf("Enter a number to test: ");
	fgets(input,100,stdin);
	sscanf(input,"%d",&test);
	printf("You wish to test the number <%d>\n", test);	
	result=nextPrime(test);
	printf("The prime after %d is %d.\n",test,result);
	return;
}
*/

//returns the first prime after given number
int nextPrime(int num)
{
	int result=0;
	while(result==0)
	{
		num++;
		result=isPrime(num);
	}
	return num;
}
