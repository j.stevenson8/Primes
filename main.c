#include "header.h"

void main()
{
	int sum,test,result;//sum is the sum of two primes---test is the number of even integers you would like to test---result if used to analyze the output of sscanf
	char input[100],extra[100];//input is used to capture user input---extra is used to for error checking user input
	int flag=0;//flag is used as a flag in my while loops

	printf("Please enter an even number you would like to see as a sum of two primes: ");
	while(flag==0)//this first while loop keeps looping until the user enters only an even integer
	{
		fgets(input,100,stdin);//used to get user input
		result=sscanf(input,"%d%s",&sum,extra);//parses the user input 
		
		if((result!=1)||(sum%2!=0)||(sum<=4))//if the user does not enter only an even integer greater than four, print out an error message and try again
		{
			printf("Error\nMust enter only an even integer\nPlease try again: ");
		}
		else//if the user enters only an even integer, ask the user to enter how many numbers they would like to see
		{
			printf("Please enter the number of even integers you want to see as the sum of two primes: ");
			while(flag==0)//this while loop keeps looping until the user enters only an integer
			{
				fgets(input,100,stdin);//captures user input
				result=sscanf(input,"%d%s",&test,extra);//parses user input
				
				if((result!=1)||(test<1))//if the user does not enter only an integer greater than one, print out an error message and try again
				{
					printf("Error\nMust enter only an integer\nPlease try again: ");	
				}
				else//if the user enters an integer greater than one, exit both while loops
				{
					flag=1;
				}	
			}
		}
	}
	int i=0;//used to count how many numbers have been printed out
	int temp;//used to do arithmetic to the sum
	while(i<test)//this while loop loops until the desired number of sums have been printed
	{
		int j=2;//used as one of the prime numbers in the sum
		flag=0;//used as a flag for the variable temp to be prime
		while(flag==0)//while temp is not prime, keep looping
		{
			temp=sum;//set temp to the sum
			j=nextPrime(j);//find the next prime number
			temp-=j;//subtract that prime number from the sum(temp)
			flag=isPrime(temp);//check to see if that number is prime
		}
		printSolution(sum,j,temp);//once you find a solution, print it out
		i++;//increment the number of sums printed out 
		sum+=2;//add two to the sum to get the next even integer
	}
	return;
}
		
