#include "header.h"

//Prints the sum of two primes for a given number
void printSolution(int num,int a,int b)
{
	printf("%d + %d = %d\n", a,b,num);
	return;
}
