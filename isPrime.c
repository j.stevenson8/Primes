#include "header.h"
/*
//This is a testbed for isPrime
void main()
{
	int test,result;
	char input[100];
	printf("Enter a number to test: ");
	fgets(input,100,stdin);
	sscanf(input,"%d",&test);
	printf("You wish to test the number <%d>\n", test);	
	result=isPrime(test);
	if(result==1)
	{
		printf("The number %d is prime.\n",test);
	}	
	else
	{
		printf("The number %d is not prime.\n",test);
	}
	return;
}
*/

//returns a 1 if the number is prime and a 0 if it is not
int isPrime(int num)
{
	for(int i=2;i*i<=num;i++)
	{
		if(num%i==0)
		{
			return 0;
		}
	}
	return 1;
}
